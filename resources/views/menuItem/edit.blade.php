@extends('admin.master')


@section('content')

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <form action="{{ route('menuItem.update', ['id' => $menuItem->id]) }}" method="post">

                        {{ method_field('PATCH') }}

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input
                                    type="text"
                                    class="form-control"
                                    id="name"
                                    name="name"
                                    value="{{ $menuItem->name }}">
                        </div>


                        {{ csrf_field() }}

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </section>
    </div>
    @include('partials.footer')

@endsection

@include('partials.scripts')

