@extends('admin.master')

@section('content')

<div class="content-wrapper">

<section class="content">

    <div class="row" style="margin-top: 80px">
        <div class="col-md-6 col-md-offset-1">
            <div class="panel panel-default">



                <div class="panel-heading text-center">Existing items</div>
                <div class="panel-body">

                    <table class="table table-striped table-hover">

                        <thead>
                        <tr>
                            <th style="text-align: center">#</th>
                            <th style="text-align: center">Name</th>
                            <th style="text-align: center">Action</th>

                        </tr>
                        </thead>
                        <tbody>

                        @foreach($menuItems as $menuItem)

                            <tr style="text-align: center">
                                <td>{{ $menuItem->id }}</td>
                                <td>{{ $menuItem->name }}</td>
                                <td>
                                    <a href="{{ route('menuItem.edit', ['id' => $menuItem->id]) }}" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                    <a href="{{ route('menuItem.delete', ['id' => $menuItem->id]) }}" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove-circle"></span> Delete</a>
                                </td>

                            </tr>

                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-3" style= " float:right; margin-right: 120px; width:20%" >
            <div class="well">
                <h2 style="padding-bottom: 25px; text-align: center"> New item</h2>
                <form action="{{ route('menuItem.store') }}" method="post">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>

                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary" style="width: 100%; margin-bottom: 15px">Create an item</button>
                </form>
            </div>
        </div>
    </div>
            </section>
    </div>

@include('partials.footer')

@endsection

@include('partials.scripts')

