@extends('layouts.master')

@section('title')

    Welcome

@endsection


@section('scripts')
    $(document).ready(function() {

        $('.slick').slick({
            speed: 300,
            slidesToShow: 5,
            adaptiveHeight: true,
            autoplay: true,
            infinite: true,
            dots: false,
            arrows: false,
            touchMove: false,
                responsive: [
                    {
                        breakpoint: 756,
                        settings: {
                        slidesToShow: 3,
                        slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 330,
                        settings: {
                        slidesToShow: 2,
                        slidesToScroll: 3
                        }
                    }
                ]
        });

        var videoSRC = $('iframe').attr('src');
        var videoSRCauto = $('iframe').attr('src') + '?autoplay=1';

        $('.launch-modal').on('click', function(e){
            e.preventDefault();
            var $modalElement = $('#' + $(this).data('modal-id'));
            $modalElement.modal();
            $('iframe').attr('src', videoSRCauto);
        });

        $('.close').click(function(){
            $('iframe').attr('src', videoSRC);
        });

        $('html').click(function(e){
            if (!$(e.target).hasClass('launch-modal')){
                $('iframe').attr('src', videoSRC);
            }
        });
    });


    $(window).scroll(function() {
        if ($(document).scrollTop() > 150) {
            $('.fixedNav').css({

                'background-color': '#414449',
                'position': 'fixed'
            });
        }
        else{
            $('.fixedNav').css({
                'background-color': 'unset',
                'position': 'relative',
                'transition': '0.7s all'
            });
        }
    });

    $('.nav .navbar-nav .navbar-right li').on('mouseenter', function(){

            $(this).addClass('active');


    });



@endsection

@section('content')


    <div class="navbar navbar-default bg" role="navigation">
        <div class="container-fluid">
            <div class="row">

            <div class="fixedNav">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <img src="{{ URL::asset('images/logo.png') }}" class="logoDesktop"/>

                    </a>
                </div>

                <div id="navbar" class="navbar-collapse collapse navbar_styling">
                    <ul class="nav navbar-nav navbar-right navBarButtons">
                        @foreach($menuItems as $menuItem)
                            <li class=""><a href="#">{{ $menuItem->name }}</a></li>
                        @endforeach
                    </ul>

                </div><!--/.nav-collapse -->
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-7">

                        <h1 class="mobileNavH1">Faster shipping with Certified Drivers</h1>
                        <h3 class="mobileNavH3">One website to manage all your transport</h3>
                        <p class="mobileNavH3Hidden">One website to manage all your logistic needs</p>
                        <p class="mobileNavH3Hidden2">Built by logistics people, for logistics people</p>
                        <h1 class="mobileNavH12">We connect shippers with carriers</h1>

                        <div class="btn-group btn-group-lg buttonsGroup" role="group" aria-label="Large button group">
                            <button type="button" class="btn btn-lg navButton1" ><span>Ship</span></button>
                            <button type="button" class="btn btn-lg navButton2" ><span>Haul</span></button>
                        </div>


                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-7">
                        <button type="button" class="btn btn-lg navButton3" ><span>Contact</span></button>
                    </div>
                </div>

            </div>
        </div>

        </div><!--/.container-fluid -->
    </div>



    <div class="container slideShow">

        <div class="row">
            <div class="col-md-12">
                <div class="slick">
                    <div><img src="{{ URL::asset('images/Ifai.png') }}" class=" firstGreyColor center-block img-responsive logoPadding"></div>
                    <div><img src="{{ URL::asset('images/FOROS.png') }}" class="greyColor center-block img-responsive logoPadding"></div>
                    <div><img src="{{ URL::asset('images/Green.png') }}" class="greyColor center-block img-responsive logoPadding"></div>
                    <div><img src="{{ URL::asset('images/BIFA.png') }}" class="greyColor center-block img-responsive logoPadding"></div>
                    <div><img src="{{ URL::asset('images/European-Road.png') }}" class="greyColor center-block img-responsive logoPadding"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="container shipAndHaul">
        <div class="row">
            <div class="col-sm-6">
                <div class="classWithPad" id="blue">
                    <div class="truck">
                        <img src="{{ URL::asset('images/ship.png') }}" class="img-responsive">
                    </div>

                    <h2 class="ship">Ship</h2>


                    <p class="desktopP">One website to book and track.<br>
                        Book transport in minutes.<br>
                        Make shipping easy.
                    </p>

                    <div class="hideBR">
                        <p class="mobileP1">One website to book and track.</p><br>
                        <p class="mobileP1"> Book transport in minutes.</p><br>
                        <p class="mobileP1"> Make shipping easy.</p><br>
                    </div>


                    <div class="btn-group btn-group-lg" role="group" aria-label="Large button group">
                        <button type="button" class="btn btn-default"><span class="boxTitleSpan">Ship</span></button>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="classWithPad" id="white">
                    <div class="haul">
                        <img src="{{ URL::asset('images/haul.png') }}">
                    </div>

                    <h2 class="haul">Haul</h2>

                    <p class="desktopP2">More work.<br>
                        Fast payment.<br>
                        No subscription.</p>


                    <p class="mobileP2">More work.</p>
                    <p class="mobileP2"> Fast payment.</p>
                    <p class="mobileP2"> No subscription.</p>



                    <div class="btn-group btn-group-lg" role="group" aria-label="Large button group">
                        <button type="button" class="btn btn-default"><span class="boxTitleSpan">
                            Carry</span></button>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="bitmap">
        <div class="container">
            <div class="row">


                <div class="col-md-6">

                    <div class="line"></div>

                    <h3 class="bitmapH3">Optimise assists all participants in the supply chain to unlock value.</h3>

                    <p>We make it easier for shippers to book and track trucks.</p>
                    <p>We make it easier for trucks to find work.</p>
                    <p>We allocate loads to empty spaces.</p>
                    <p>Real time visibility of all trucks on one platform, communicate with the click of a finger.</p>
                    <p> Our in-house technology removes manual processing allowing us to pass on savings to you.</p>
                </div>

                <div class="col-md-6 col-md-push-4 bitIMG">

                    <img src="{{ URL::asset('images/M_Bitmap.png') }}" class="img-responsive NewBIT ">
                    <img src="{{ URL::asset('images/screen.png') }}" class="img-responsive oldBIT center-block">

                </div>

            </div>
        </div>
    </div>


    <div class="clouds">
        <div class="container">
            <div class="row iframeReplace">

                <div class="col-md-6 col-md-push-6">
                    <div class="line" style="margin-bottom: 21px"></div>
                    <h2>Ship with optimise</h2>
                    <h1 class="newH1">Efficient, Reliable and Data-driven</h1>
                    <h1 class="oldH1">Fast, Reliable, Great Rates, Total Visibility</h1>
                </div>

                <div class="col-md-6 col-md-pull-6" style="margin-bottom: -30px">
                    <img src="{{ URL::asset('images/video.png') }}" class="img-responsive center-block launch-modal" id = "videoReplace" data-modal-id="modal-video"/>
                </div>

                <div class="modal fade" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="modal-video-label" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="modal-video">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ObcCEVTZsvo" allowfullscreen>

                                        </iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container widget">
        <div class="row moreWorkFastPayment">
            <div class="line center-block"></div>

            <div class="belowVid">
                <div class="col-md-7 col-md-offset-3 text-center">
                    <h1 class="desktopWork">More Work, Fast Payment.</h1>
                    <h1 class="mobileWork">Fast, Reliable, Great Rates, Total Visibility</h1>
                    <h3>Sign up, log into our easy to use webpage to brook trucks instantly, track all your loads real time, instant notifications.</h3>
                </div>
            </div>
        </div>


        <div class="row" style="padding-top: 75px">

            <div class="col-xs-12 col-md-12 col-lg-3">
                <div class="marginCorrection">
                    <img id = "" src="{{ URL::asset('images/Umbrella.png') }}" class="img-responsive"/>
                    <h3 class="">Great rates</h3>
                    <p>Move loads in unused capacity. Our highly automated technology allows us to pass on savings to you.</p>

                </div>
            </div>

            <div class="col-xs-12 col-md-3 bye">
                <div class="clock marginCorrection">
                    <img src="{{ URL::asset('images/Clock.png') }}" class="img-responsive"/>
                    <h3>Save time</h3>
                    <p>Find loads instantly. View every carrier on one single dashboard, control your transport with a click.</p>

                </div>
            </div>

            <div class="col-xs-12 col-md-3 bye">
                <div class="badgeW marginCorrection">
                    <img src="{{ URL::asset('images/Badge.png') }}" class="img-responsive"/>
                    <h3>Reliable</h3>
                    <p>Certified trusted drivers. Insurance, licensing and businesss references  verified. By shippers like you.</p>

                </div>
            </div>
            <div class="col-xs-12 col-md-3 bye">
                <div class="eyeglasses marginCorrection">
                    <img src="{{ URL::asset('images/Eyeglass.png') }}" class="img-responsive" style="padding-top: 42px"/>
                    <h3>Full visibility</h3>
                    <p>Track loads real time, notifications sent to keep you up to date.</p>

                </div>
            </div>
        </div>


        <div class="row hiddenWidget">

            <div class="line center-block" style="margin-top: 53px "></div>
            <div class="col-md-12 text-center">

                <h1>More Work, Fast Payment</h1>
                <h3>No Monthly Subscriptions<br>
                    No Reverse Bidding</h3>

            </div>

        </div>


        <div class="row walletBackgroundChange">

            <div class="col-xs-12 col-md-12 wallet">
                <div class="marginCorrection">
                    <img src="{{ URL::asset('images/Wallet.png') }}" class="img-responsive"/>
                    <h3>Fair price</h3>
                    <p>Fair market rates calculated by us. No reverse bidding or haggling. Gain extra shipments to fill empty space.</p>
                </div>
            </div>


            <div class="col-xs-12 col-md-3 bye">
                <div class="speedometer marginCorrection">
                    <img src="{{ URL::asset('images/Speedometer.png') }}" class="img-responsive"/>
                    <h3>Fast payment</h3>
                    <p>Receive payment in just a few short days. No more chasing outstanding invoices.</p>

                </div>
            </div>

            <div class="col-xs-12 col-md-3 bye">
                <div class="directions marginCorrection">
                    <img src="{{ URL::asset('images/Directions.png') }}" class="img-responsive"/>
                    <h3>Track</h3>
                    <p>Track all your trucks realtime on our mobile app or webpage.</p>

                </div>
            </div>

            <div class="col-xs-12 col-md-3 bye">
                <div class="user marginCorrection">
                    <img src="{{ URL::asset('images/User.png') }}" class="img-responsive"/>
                    <h3>Less Admin</h3>
                    <p>Documentation automatically produced, less resources tied up in admin.</p>

                </div>
            </div>

        </div>

    </div>

    <div class="container-fluid talk">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>Talk to our team</p>
                    <h1>No more wasted time baby sitting loads, track everything real time and order trucks in seconds</h1>
                    <button type="button" class="btn btn-lg navButtonTalk"><span>Talk to our team</span></button>
                </div>

            </div>
        </div>
    </div>

    <div class="container-fluid contactMe">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="line center-block"></div>
                    <h1>Contact us to get started</h1>
                    <h4>Drop us an e-mail at</h4>
                    <p class="email">sales&#64;optimise.com</p>
                    <h5>or use the formular below</h5>
                </div>
            </div>

            <div class="row desktopContactMe" style="margin-top: 55px">

                    <form>
                        <div class="col-md-4">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name">

                        </div>
                        <div class="col-md-4">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email">
                        </div>
                        <div class="col-md-4">
                            <label for="company">Company</label>
                            <input type="text" class="form-control" id="company">
                        </div>

                        <div class="col-md-12 details">
                            <label for="details">Additional details</label>
                            <input type="text" class="form-control" id="details">
                        </div>
                    </form>


                    <button type="button" class="btn btn-lg navButtonContact center-block"><span>Get in touch</span></button>
                    <button type="button" class="btn btn-lg navButtonContactHidden center-block"><span>Get in touch</span></button>

            </div>
        </div>

        <div class="row mobileContactMe">

            <form>
                <div class="col-md-4">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name">

                </div>
                <div class="col-md-4">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email">
                </div>
                <div class="col-md-4">
                    <label for="company">Company</label>
                    <input type="text" class="form-control" id="company">
                </div>

                <div class="col-md-12 details">
                    <label for="details">Additional details</label>
                    <input type="text" class="form-control" id="details">
                </div>
            </form>


            <button type="button" class="btn btn-lg navButtonContact center-block"><span>Get in touch</span></button>
            <button type="button" class="btn btn-lg navButtonContactHidden center-block"><span>Get in touch</span></button>


        </div>


    </div>

    <div class="container-fluid footing">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <img src="{{ URL::asset('images/Phone.png') }}" alt="Phone" class="img-responsive phone">
                    <p>Phone</p>
                    <h5>UK: +44 (0)20 8935 5460<br>
                        Europe: +353 (0)1 910 9978
                    </h5>
                </div>
                <div class="col-md-4">
                    <img src="{{ URL::asset('images/Location.png') }}" alt="Location" class="img-responsive location">
                    <p>UK Address</p>
                    <h5>Optimise<br>
                        Cutlers Court<br>
                        115 Houndsditch<br>
                        London EC3A 7BR
                    </h5>
                </div>
                <div class="col-md-4">
                    <img src="{{ URL::asset('images/Location.png') }}" alt="Location HQ" class="img-responsive location2">
                    <p>Headquarters</p>
                    <h5>Optimise<br>
                        D5 Riverview Business Park<br>
                        Nangor Road<br>
                        Dublin 12
                    </h5>

                    <div class="social">
                        <img src="{{ URL::asset('images/02_facebook.png') }}" alt="FB" class="img-responsive">
                    </div>
                    <div class="social">
                        <img src="{{ URL::asset('images/07_linkedin.png') }}" alt="LN" class="img-responsive">
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid copyright">
        <div class="row">
            <div class="col-md-12 text-center">
                <p> © 2017 Optimise</p>
            </div>
        </div>
    </div>


@endsection