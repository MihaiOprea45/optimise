<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [

    'uses' => 'MainPageController@index',
    'as' => 'main.index'

]);


Auth::routes();

Route::resource('admin', 'AdminController', ['except' => 'create']);

Route::get('menu', [

    'uses' => 'menuItemController@index',
    'as' => 'menuItem.index'

]);

Route::post('menu', [
    'uses' => 'menuItemController@store',
    'as' => 'menuItem.store'
]);

Route::get('menu/{id}/edit', [
    'uses' => 'menuItemController@edit',
    'as' => 'menuItem.edit'
]);

Route::patch('menu/{id}', [
    'uses' => 'menuItemController@update',
    'as' => 'menuItem.update'
]);


Route::get('menu/{id}/delete', [
    'uses' => 'menuItemController@destroy',
    'as' => 'menuItem.delete'
]);